from django.db import models
from django.conf import settings 
from django.utils import timezone

# Create your models here.

# CLASS define el modelo/objeto llamado POST
class Post(models.Model):
    # Se define relacion (link) con otro modelo
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    # Se define un texto con numero limitados de caracteres
    title = models.CharField(max_length=200)
    # Se define un texto sin limites de caracteres
    text = models.TextField()
    # Se define fecha y hora
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

