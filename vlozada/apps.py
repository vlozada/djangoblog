from django.apps import AppConfig


class VlozadaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'vlozada'
